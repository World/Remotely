/* remotely-app.vala
 *
 * Copyright (C) 2018 Felix Häcker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

int main (string[] args) {
	var app = new Gtk.Application ("de.haeckerfelix.Remotely", ApplicationFlags.FLAGS_NONE);

	var action = new GLib.SimpleAction ("about", null);
	action.activate.connect (() => { show_about_dialog (app.active_window); });
    app.add_action (action);

    action = new GLib.SimpleAction ("new-window", null);
	action.activate.connect (() => { new_window(app); });
    app.add_action (action);

	app.activate.connect (() => { new_window(app); });

	return app.run (args);
}

private void new_window(Gtk.Application app){
	var win = new Remotely.Window (app);
	win.present ();
}

private void show_about_dialog(Gtk.Window window){
    string[] authors = { "Felix Häcker <haeckerfelix@gnome.org>" };
    string[] artists = { "Tobias Bernard" };

	Gtk.show_about_dialog (window,
		"artists", artists,
	    "authors", authors,
		"program-name", "Remotely",
		"license-type", Gtk.License.GPL_3_0,
		"logo-icon-name", "de.haeckerfelix.Remotely",
		"version", "1.0",
		"comments", _("A VNC viewer"),
		"website", "https://gitlab.gnome.org/World/Remotely ",
		"website-label", _("Project Homepage"));
}
